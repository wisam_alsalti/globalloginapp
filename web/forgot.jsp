<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <link rel="stylesheet" type="text/css" href="./css/logued.css">
        <link rel="stylesheet" type="text/css" href="./css/forgot.css">
    </head>

    <body>
        <div id="general">
            <%@include file="/WEB-INF/jspf/header.jspf" %>
            <div id=cuerpo>
                <div id=subCuerpo>

                    <div id=titulo>
                        <h3>�Qu� problema est�s teniendo con tu cuenta de Global Login?</h3>
                    </div>
                    <div id=contenido>
                        <form action="Forgot" method="POST">
                            <ul>
                                <li>
                                    <label for="mail">Email: </label>
                                    <input type="email" id="mail" name="mail" placeholder="ejemplo@dominio.com" required="" style="width:150px;" />
                                </li>
                                <li>
                                    <input type="radio" id="a" name="opcion" value="pass" required="" />
                                    <label for="a">He olvidado mi contrase�a.</label>
                                </li>
                                <li>
                                    <input type="radio" id="b" name="opcion" value="username" />
                                    <label for="b">He olvidado mi nombre de usuario.</label>
                                </li>
                                <li>
                                    <input type="radio" id="c" name="opcion" value="all" />
                                    <label for="c">He olvidado ambas cosas.</label>
                                </li>
                                <input class="boton" type="button" onclick=" location.href = 'http://localhost:8080/PrimeraVersion/login.jsp'" value="Cancelar" />
                                <input class="boton" type="submit" value="Siguiente" />

                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
