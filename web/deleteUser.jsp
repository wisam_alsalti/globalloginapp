<%-- 
    Document   : opciones
    Created on : 20-abr-2014, 1:08:00
    Author     : WiSaM
--%>

<%@page import="Model.Aplicacion"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Model.User"%>
<%@page import="Utils.Util"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="BBDD.ConsultaBD"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Util utilb = new Util();
    HttpSession sesion = request.getSession(false);
    String user = "";
    String app = "";

    if (sesion.getAttribute("User") == null) {
        response.sendRedirect("login.jsp");
    } else if (!utilb.isAdmin((String) sesion.getAttribute("User"))) {
        response.sendRedirect("logued.jsp");
    } else {

        user = (request.getParameter("username") != null) ? request.getParameter("username") : "";
        app = (request.getParameter("app") != "---") ? request.getParameter("app") : "---";

        ConsultaBD conBD = new ConsultaBD();
        ArrayList<User> users = new ArrayList<User>();
        users = conBD.getUserListByCriteria(user, app, "", "");

        ArrayList<Aplicacion> apps = new ArrayList<Aplicacion>();
        apps = conBD.getAplicaciones();
%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./css/logued.css">
        <link rel="stylesheet" type="text/css" href="./css/forgot.css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    </head>
    <body>
        <div id="general">
            <%@include file="/WEB-INF/jspf/header.jspf" %>
            <div id=cuerpo>
                <div id=subCuerpoR>
                    <div id=titulo>
                        <h3>Baja</h3>
                    </div>
                    <div id=contenido>
                        <form action="DeleteUser" method="POST">
                            <ul>
                                <li>
                                    <label for="user">Usuario: </label>
                                    <input type="text" id="user" name="user" value="<%=user%>" readonly/>
                                </li>
                                <li>
                                    <label for="app">Aplicación: </label>
                                    <input type="text" id="app" name="app" value="<%=app%>" readonly/>
                                </li>
                                <li>
                                    <label for="tipBloq">Dar de baja: </label>
                                    <select id="tipBloq" name="tipBloq" required>
                                        <%if(!app.equals("---")){%>
                                        <option value="suscripcion">Solo suscripción</option>
                                        <%}%>
                                        <option value="usuario">Usuario y suscripciones</option>
                                    </select>
                                </li>
                                <li>
                                    <input class="boton" type="button" onclick=" location.href = 'http://localhost:8080/PrimeraVersion/cpanel.jsp'" value="Cancelar" />
                                    <input class="boton" type="submit" value="Aceptar"/>
                                </li>
                            </ul>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<%}%>