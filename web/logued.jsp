<%-- 
    Document   : logued
    Created on : 11-mar-2014, 21:04:44
    Author     : WiSaM
--%>

<%@page import="Model.Aplicacion"%>
<%@page import="org.apache.jasper.tagplugins.jstl.ForEach"%>
<%@page import="java.util.ArrayList"%>
<%@page import="BBDD.ConsultaBD"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    HttpSession sesion = request.getSession(false);

    //No permitir que el browser guarde en cache esta pagina, para evitar
    //el uso del botón de "Atras".
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", 0);

    if (sesion.getAttribute("User") == null) {
        response.sendRedirect("login.jsp");
    }
%>

<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./css/logued.css">
    </head>
    <body>
        <div id="general">
            <%@include file="/WEB-INF/jspf/header.jspf" %>
            <div id=cuerpo>
                <div id='subCuerpo'>
                    <%@include file="/WEB-INF/jspf/navi.jspf" %>

                    <br><br><br>

                    <section>
                        <p>Está suscrito a:</p>
                        <ul>
                            <%
                                ConsultaBD conBD = new ConsultaBD();
                                ArrayList<Aplicacion> listaApps = conBD.getAplicacionesByNombreUsuario((String) sesion.getAttribute("User"));
                                if (listaApps.isEmpty()) {
                                    out.println("No está suscrito a ninguna página web.");
                                } else {
                                    for (Aplicacion app : listaApps) {
                                        out.println("<li><a href=\"" + app.getUrl() + "\" target=\"_blank\">" + app.getNombre() + "</a></li>");

                                    }
                                }
                            %>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </body>
</html>
