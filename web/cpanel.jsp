<%-- 
    Document   : opciones
    Created on : 20-abr-2014, 1:08:00
    Author     : WiSaM
--%>

<%@page import="Model.Aplicacion"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Model.User"%>
<%@page import="Utils.Util"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="BBDD.ConsultaBD"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Util utilb = new Util();
    HttpSession sesion = request.getSession(false);
    String user = "";
    String app = "";
    String perfI = "";
    String perfE = "";

    if (sesion.getAttribute("User") == null) {
        response.sendRedirect("login.jsp");
    } else if (!utilb.isAdmin((String) sesion.getAttribute("User"))) {
        response.sendRedirect("logued.jsp");
    } else {
        if (request.getParameter("submit")!=null){
            user = (request.getParameter("userName")!=null)?request.getParameter("userName"):"";
            app = (request.getParameter("app")!=null&&(!request.getParameter("app").equals("NS")))?request.getParameter("app"):"";
            perfI = (request.getParameter("perfI")!=null&&(!request.getParameter("perfI").equals("0")))?request.getParameter("perfI"):"";
            perfE = (request.getParameter("perfE")!=null&&(!request.getParameter("perfE").equals("0")))?request.getParameter("perfE"):"";
        }
        
        ConsultaBD conBD = new ConsultaBD();
        ArrayList<User> users = new ArrayList<User>();
        users = conBD.getUserListByCriteria(user, app, perfI, perfE);
        
        ArrayList<Aplicacion> apps = new ArrayList<Aplicacion>();
        apps = conBD.getAplicaciones();
%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./css/opciones.css">
        <link rel="stylesheet" type="text/css" href="./css/tabla.css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-latest.js"></script> 
        <script type="text/javascript" src="js/jquery.tablesorter.js"></script> 
    </head>
    <body>
        <script>
            $(document).ready(function() {
                $("table").tablesorter({
                    // pass the headers argument and assing a object 
                    headers: {
                        // assign the third column (we start counting zero) 
                        5: {
                            // disable it by setting the property sorter to false 
                            sorter: false
                        }
                    }
                });
            });
            
             function confirmUser(page, user, app){
                var ask=confirm("¿Estás seguro?");
                if(ask){
                  window.location= "/" + page + "?username=" + user + "&app=" + app;
                 }
            }
        </script>

        <div id="general">
            <%@include file="/WEB-INF/jspf/header.jspf" %>
            <div id=cuerpo>
                <div id='subCuerpo'>
                    <%@include file="/WEB-INF/jspf/navi.jspf" %>

                    <div id="contenidoSubCuerpo">
                        <div id="opciones">
                            <div id="tituloOpciones">
                                <span>Filtros</span>
                            </div>
                            <div id="listaOpciones">
                                <form action="cpanel.jsp" method="POST">
                                    <input type="text" name="userName" placeholder="Usuario" value="" pattern="^[a-zA-Z][a-zA-Z0-9_-]{3,16}$" title="Su nombre de usuario debe ser alfanumérico, 4 carácteres mínimo y 16 como máximo (Ex. User_123)" />
                                    <select name="app">
                                        <option selected value="NS"> Elige una aplicación </option>
                                        <% 
                                        for (int i = 0; i < apps.size(); i++) {
                                        %>
                                        
                                        <option value="<%=apps.get(i).getNombre()%>"><%=apps.get(i).getNombre()%></option>
                                        
                                        <%}%>
                                    </select>
                                    <select name="perfI">
                                        <option selected value="0"> Elige un perfil interno </option>
                                        <option value="15">Admin</option>
                                        <option value="4">Aplicación</option>
                                        <option value="2">Premium</option>
                                        <option value="1">Free</option>
                                    </select>
                                    <select name="perfE">
                                        <option selected value="0"> Elige un perfil externo </option>
                                        <option value="1">Perfil 1</option>
                                        <option value="2">Perfil 2</option>
                                        <option value="3">Perfil 3</option>
                                        <option value="4">Perfil 4</option>
                                        <option value="5">Perfil 5</option>
                                        <option value="6">Perfil 6</option>
                                        <option value="7">Perfil 7</option>
                                        <option value="8">Perfil 8</option>
                                        <option value="9">Perfil 9</option>
                                        <option value="10">Perfil 10</option>
                                        <option value="11">Perfil 11</option>
                                        <option value="12">Perfil 12</option>
                                        <option value="13">Perfil 13</option>
                                        <option value="14">Perfil 14</option>
                                        <option value="15">Perfil 15</option>
                                    </select>
                                    <input type="submit" name="submit" value="Aplicar filtros" >
                                </form>
                                    <ul>
                                     <li>
                                        <a href="register.jsp">Registrar nuevo usuario</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="contenidoOpcion">
                            <div id="tabla" style="overflow:auto;height:100%;position:relative;">
                                <%
                                    if (!users.isEmpty()) {

                                %>

                                <table id="myTable" class="tablesorter"> 
                                    <thead> 
                                        <tr> 
                                            <th class="header">Usuario</th> 
                                            <th class="header">Email</th> 
                                            <th class="header">Perfil Interno</th> 
                                            <th class="header">Aplicación</th> 
                                            <th class="header">Perfil Externo</th> 
                                            <th>Editar</th>
                                           
                                        </tr> 
                                    </thead> 
                                    <tbody> 
                                        <% 
                                        for (int i = 0; i < users.size(); i++) {
                                        %>
                                        <tr> 
                                            <td><%=users.get(i).getName()%></td> 
                                            <td><%=users.get(i).getEmail()%></td> 
                                            <td><%=users.get(i).getPerfil()%></td> 
                                            <td><%=(users.get(i).getApp() != null) ? users.get(i).getApp() : "---"%></td> 
                                            <td><%=users.get(i).getPerfilExt()%></td> 
                                            <td><a href="edit.jsp?name=<%=users.get(i).getName()%>&email=<%=users.get(i).getEmail()%>&perfI=<%=users.get(i).getPerfil()%>&app=<%=(users.get(i).getApp() != null) ? users.get(i).getApp() : "---"%>&perfE=<%=users.get(i).getPerfilExt()%>">Editar</a></td> 
                                            <%if (users.get(i).getApp() != null){%>                                            
                                            <td><a href="#" onclick="confirmUser('PrimeraVersion/bloquearUser.jsp','<%=users.get(i).getName()%>','<%=(users.get(i).getApp() != null) ? users.get(i).getApp() : "---"%>')">Bloquear</a></td> 
                                            <%}else{%>
                                            <td><a href="#" onclick="alert('El usuario no está suscrito a ninguna aplicación.')">Bloquear</a></td> 
                                            <%}%>
                                            <td><a href="#" onclick="confirmUser('PrimeraVersion/deleteUser.jsp','<%=users.get(i).getName()%>','<%=(users.get(i).getApp() != null) ? users.get(i).getApp() : "---"%>')">Eliminar</a></td> 
                                        </tr> 
                                        <%}%>

                                    </tbody> 
                                </table>
                                <% } else {               
                                %>
                                <span>No existen datos a mostrar.</span>
                                <%}%>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<%}%>