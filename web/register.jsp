<%-- 
    Document   : register
    Created on : 07-abr-2014, 16:57:43
    Author     : WiSaM
--%>


<%@page import="Utils.Util"%>
<%@page import="Model.Aplicacion"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Model.User"%>
<%@page import="BBDD.ConsultaBD"%>
<%
    Util utilb = new Util();
    HttpSession sesion = request.getSession(false);
    ConsultaBD conBD = new ConsultaBD();

    ArrayList<Aplicacion> apps = new ArrayList();
    apps = conBD.getAplicaciones();
    Aplicacion refererApp = null;

    //Comprobar si se viene desde una aplicaci�n.
    if (sesion.getAttribute("Referer") != null) {
        String referer = sesion.getAttribute("Referer").toString();
        for (int i = 0; i < apps.size(); i++) {
            if (referer.contains(apps.get(i).getUrl())) {
                refererApp = apps.get(i);
            }
        }
    }

   // if (sesion.getAttribute("User") != null) {
        //Si es usuario admin o el usuario viene de una pagina que tenga activado el registro
        if (utilb.isAdmin((String) sesion.getAttribute("User")) || refererApp!=null) {
 
%>           


<script type="text/javascript">
    function validatePass(p1, p2) {
        if (p1.value !== p2.value || p1.value === '' || p2.value === '') {
            p2.setCustomValidity('Las contrase�as no coinciden');
        } else {
            p2.setCustomValidity('');
        }
    }
</script>


<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <link rel="stylesheet" type="text/css" href="./css/logued.css">
        <link rel="stylesheet" type="text/css" href="./css/forgot.css">
    </head>

    <body>
        <div id="general">
            <%@include file="/WEB-INF/jspf/header.jspf" %>
            <div id=cuerpo>
                <div id=subCuerpoR>

                    <div id=titulo>
                        <h3>Registro</h3>
                    </div>
                    <div id=contenido>
                        <form action="Register" method="POST">
                            <table border="0">
                                <tbody>
                                    <tr>
                                        <td>Usuario</td>
                                        <td><input type="text" name="userName" value="" pattern="^[a-zA-Z][a-zA-Z0-9_-]{3,16}$" title="Su nombre de usuario debe ser alfanum�rico, 4 car�cteres m�nimo y 16 como m�ximo (Ex. User_123)" required="" /></td>
                                    </tr>
                                    <tr>
                                        <td>Contrase�a</td>
                                        <td><input id="p1" type="password" name="pass" value="" pattern="[a-zA-Z0-9_-]{4,16}" required="" title="M�nimo 4 car�cteres" /></td>
                                    </tr>
                                    <tr>
                                        <td>Confirmar Contrase�a</td>
                                        <td><input id="p2" type="password" name="pass1" value="" pattern="[a-zA-Z0-9_-]{4,16}" required="" oninput="validatePass(document.getElementById('p1'), this);" onfocus="validatePass(document.getElementById('p1'), this);" title="M�nimo 4 car�cteres" /></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><input type="email" name="email" value="" placeholder="ejemplo@dominio.com" autocomplete="on" required="" /></td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <td>Aplicaci�n</td>
                                        <%
                                        //Listar todos las apps (caso de admin)
                                        if (refererApp==null){%>
                                <td>        
                                <select name="app" required>
                                                    <%
                                                        for (int i = 0; i < apps.size(); i++) {
                                                    %>

                                                    <option value="<%=apps.get(i).getNombre()%>"><%=apps.get(i).getNombre()%></option>

                                                    <%}%>
                                        </select>
                                        </td>
                                        <%}else{%>
                                        
                                <td><input type="text" name="app" value="<%=refererApp.getNombre()%>" readonly /></td>
                                        <%}%>
                                    </tr>
                                    
                                    <tr>
                                        <td>Hint</td>
                                        <td><input type="text" name="hint" value="" maxlength="50" placeholder="" autocomplete="off" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="button" onclick=" location.href = 'http://localhost:8080/PrimeraVersion/login.jsp'" value="Cancelar" /></td>
                                        <td><input type="submit" value="Aceptar" onclick="validatePass(document.getElementById('p1'), document.getElementById('p2'));" /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
 <%           
            
        } else {
            response.sendRedirect("logued.jsp");
        }
   // }
%>
