function getHintAlert() {
    var email = prompt('Email', 'example@domain.com');
    if (email !== '' && email !== null) {
        location.href = 'hint.jsp?email=' + email;
    } else if (email !== null) {
        alert('Has de introducir un email válido.');
    }
}

function responseHintAlert(msg) {
    alert(msg);
    location.href = 'login.jsp';
}

function cargar(div, desde) {
    $(div).load(desde);
}

function validatePass(p1, p2) {
    if (p1.value !== p2.value || p1.value === '' || p2.value === '') {
        p2.setCustomValidity('Las contraseñas no coinciden');
    } else {
        p2.setCustomValidity('');
    }
}
