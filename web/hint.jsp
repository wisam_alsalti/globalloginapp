<%-- 
    Document   : hint
    Created on : 19-abr-2014, 12:14:22
    Author     : WiSaM
--%>

<%@page import="Utils.Util"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
Util util = new Util();
String email = request.getParameter("email");
String username = util.getUserByEmail(email);
String msg = "";
if (username == null){
    msg = "El email no corresponde a ninguna cuenta.";
}else{
    String hint = util.getHintByUser(username);
    if (hint == null){
        msg = username + ", no has proporcionado ningún hint.";
    }else{
        msg = "Hint:\\n" + hint;
    }
}

%>
        
<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript" src="js/scripts.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Global Login</title>
    </head>
    <body onload="responseHintAlert('<%=msg%>');">

    </body>
</html>
