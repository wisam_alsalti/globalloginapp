<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HttpSession sesion = request.getSession(false);

    if ((sesion.getAttribute("User") == null) && (request.getParameter("codePass") == null)) {
        response.sendRedirect("login.jsp");
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <link rel="stylesheet" type="text/css" href="./css/logued.css">
        <link rel="stylesheet" type="text/css" href="./css/forgot.css">
    </head>
    <body>
        <script type="text/javascript">
            function validatePass(p1, p2) {
                if (p1.value !== p2.value || p1.value === '' || p2.value === '') {
                    p2.setCustomValidity('Las contraseñas no coinciden');
                } else {
                    p2.setCustomValidity('');
                }
            }
        </script>
        <div id="general">
            <%@include file="/WEB-INF/jspf/header.jspf" %>
            <div id=cuerpo>
                <div id=subCuerpo>

                    <div id=titulo>
                        <h3>¡Generar una nueva contraseña!</h3>
                    </div>
                    <div id=contenido>
                        <form action="ChangePass" method="POST">
                            <%
                                String codePass = request.getParameter("codePass");
                                if (codePass != null) {%>
                            <input type="hidden" name="cambioPassCode" value="<%=codePass%>" />
                            <%}%>
                            <ul>

                                <% if (sesion.getAttribute("User") != null) {%>
                                <li>
                                    <p>Contraseña actual</p></td>
                                    <input id="pA" type="password" name="passAct" value="" pattern="[a-zA-Z0-9_-]{4,16}" required="" title="Mínimo 4 carácteres" />
                                </li>
                                <%}%>
                                <li>
                                    <p>Nueva contraseña</p></td>
                                    <input id="p1" type="password" name="pass" value="" pattern="[a-zA-Z0-9_-]{4,16}" required="" title="Mínimo 4 carácteres" />
                                </li>
                                <li>
                                    <p>Confirmar contraseña</p></td>
                                    <input id="p2" type="password" name="pass1" value="" pattern="[a-zA-Z0-9_-]{4,16}" required="" oninput="validatePass(document.getElementById('p1'), this);" onfocus="validatePass(document.getElementById('p1'), this);" title="Mínimo 4 carácteres" />
                                </li>

                                <input type="button" onclick=" location.href = 'http://localhost:8080/PrimeraVersion/login.jsp'" value="Cancelar" /> 
                                <input type="submit" value="Aceptar" /></td>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
