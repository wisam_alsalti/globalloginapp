<%-- 
    Document   : opciones
    Created on : 20-abr-2014, 1:08:00
    Author     : WiSaM
--%>

<%@page import="Model.Aplicacion"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Model.User"%>
<%@page import="Utils.Util"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="BBDD.ConsultaBD"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Util utilb = new Util();
    HttpSession sesion = request.getSession(false);
    String user = "";
    String email = "";
    String app = "";
    String perfI = "";
    String perfE = "";

    if (sesion.getAttribute("User") == null) {
        response.sendRedirect("login.jsp");
    } else if (!utilb.isAdmin((String) sesion.getAttribute("User"))) {
        response.sendRedirect("logued.jsp");
    } else {
        
        user = (request.getParameter("name") != null) ? request.getParameter("name") : "";
        email = (request.getParameter("email") != null) ? request.getParameter("email") : "";
        app = (request.getParameter("app") != null && (!request.getParameter("app").equals("NS"))) ? request.getParameter("app") : "";
        //perfI = (request.getParameter("perfI") != null && (!request.getParameter("perfI").equals("0"))) ? request.getParameter("perfI") : "";
        //perfE = (request.getParameter("perfE") != null && (!request.getParameter("perfE").equals("0"))) ? request.getParameter("perfE") : "";

        ConsultaBD conBD = new ConsultaBD();
        ArrayList<User> users = new ArrayList<User>();
        users = conBD.getUserListByCriteria(user, app, perfI, perfE);

        ArrayList<Aplicacion> apps = new ArrayList<Aplicacion>();
        apps = conBD.getAplicaciones();
%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./css/opciones.css">
        <link rel="stylesheet" type="text/css" href="./css/tabla.css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-latest.js"></script> 
        <script type="text/javascript" src="js/jquery.tablesorter.js"></script> 
    </head>
    <body>
        <div id="general">
            <%@include file="/WEB-INF/jspf/header.jspf" %>
            <div id=cuerpo>
                <div id='subCuerpo'>
                    <%@include file="/WEB-INF/jspf/navi.jspf" %>

                    <div id="contenidoSubCuerpo">
                        <div id="opciones">
                            <div id="tituloOpciones">
                                <span>Editar</span>
                            </div>
                            <div id="listaOpciones">

                            </div>
                        </div>
                        <div id="contenidoOpcion">
                            <form action="Edit" method="POST">
                                <input type="text" name="oldName" value="<%=user%>" hidden/>
                                <input type="text" name="oldEmail" value="<%=email%>" hidden/>
                                <table border="0">
                                    <tbody>
                                        <tr>
                                            <td>Usuario</td>
                                            <td><input type="text" name="userName" value="<%=user%>" pattern="^[a-zA-Z][a-zA-Z0-9_-]{3,16}$" title="Su nombre de usuario debe ser alfanumérico, 4 carácteres mínimo y 16 como máximo (Ex. User_123)" required="" /></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td><input type="email" name="email" value="<%=email%>" placeholder="ejemplo@dominio.com" autocomplete="on" required="" /></td>
                                        </tr>
                                        <tr>
                                            <td>Perfil Interno</td>
                                            <td>
                                                <select name="perfI">
                                                    <option selected value="0"> Elige un perfil interno </option>
                                                    <option value="15">Admin</option>
                                                    <option value="4">Aplicación</option>
                                                    <option value="2">Premium</option>
                                                    <option value="1">Free</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Aplicación</td>
                                            <td><input type="text" name="app" value="<%=app%>" required="" readonly/></td>
<!--                                            <td>
                                                <select name="appA">
                                                    <option selected value="NS"> Elige una aplicación </option>
                                                    <%
                                                        for (int i = 0; i < apps.size(); i++) {
                                                    %>

                                                    <option value="<%=i+1%>"><%=apps.get(i).getNombre()%></option>

                                                    <%}%>
                                                </select>

                                            </td>-->
                                        </tr>
                                        <tr>
                                            <td>Perfil Externo</td>
                                            <td>
                                                <select name="perfE">
                                                    <option selected value="0"> Elige un perfil externo </option>
                                                    <option value="1">Perfil 1</option>
                                                    <option value="2">Perfil 2</option>
                                                    <option value="3">Perfil 3</option>
                                                    <option value="4">Perfil 4</option>
                                                    <option value="5">Perfil 5</option>
                                                    <option value="6">Perfil 6</option>
                                                    <option value="7">Perfil 7</option>
                                                    <option value="8">Perfil 8</option>
                                                    <option value="9">Perfil 9</option>
                                                    <option value="10">Perfil 10</option>
                                                    <option value="11">Perfil 11</option>
                                                    <option value="12">Perfil 12</option>
                                                    <option value="13">Perfil 13</option>
                                                    <option value="14">Perfil 14</option>
                                                    <option value="15">Perfil 15</option>
                                                </select>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="button" onclick=" location.href = 'http://localhost:8080/PrimeraVersion/cpanel.jsp'" value="Cancelar" /></td>
                                            <td><input type="submit" name="submit" value="Aceptar"/></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<%}%>