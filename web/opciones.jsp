<%-- 
    Document   : opciones
    Created on : 20-abr-2014, 1:08:00
    Author     : WiSaM
--%>

<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="BBDD.ConsultaBD"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HttpSession sesion = request.getSession(false);

    if (sesion.getAttribute("User") == null) {
        response.sendRedirect("login.jsp");
    } else {

        ConsultaBD conBD = new ConsultaBD();
        String hint = "";
        hint = conBD.getHintByUser(sesion.getAttribute("User").toString());

%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./css/opciones.css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    </head>
    <body>
        <div id="general">
            <%@include file="/WEB-INF/jspf/header.jspf" %>
            <div id=cuerpo>
                <div id='subCuerpo'>
                    <%@include file="/WEB-INF/jspf/navi.jspf" %>

                    <div id="contenidoSubCuerpo">
                        <div id="opciones">
                            <div id="tituloOpciones">
                                <span>Opciones</span>
                            </div>
                            <div id="listaOpciones">
                                <ul>
                                    <li>
                                        <a href="opciones.jsp?act=cambiaPass">Cambiar Contraseña</a>
                                    </li>
                                    <li>
                                        <a href="opciones.jsp?act=cambiaHint">Cambiar Hint</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="contenidoOpcion">
                            <%                                    String hintPage = "";
                                    if (hint.isEmpty()) {
                                        hintPage = "Tu cuenta no tiene Hint";
                                    } else {
                                        hintPage = "<p>Hint actual: <span>" + hint + "</span></p>";
                                    }
                                    String act = request.getParameter("act");
                                    if (act != null) {
                                        if (act.equals("cambiaPass") || act.equals("cambiaHint")) {
                                            if (act.equals("cambiaHint")) {
                                                out.println(hintPage);
                                            }
                                            InputStream con = application.getResourceAsStream("/WEB-INF/jspf/" + act + ".jspf");
                                            BufferedReader in = new BufferedReader(new InputStreamReader(con));
                                            String inputLine;
                                            while ((inputLine = in.readLine()) != null) {
                                                out.println(inputLine);
                                            }
                                            in.close();
                                        } else {
                                            out.println("");
                                        }
                                    }
                                }
                            %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
