<%-- 
    Document   : login
    Created on : 11-mar-2014, 21:02:42
    Author     : WiSaM
--%>

<%@page import="BBDD.ConsultaBD"%>
<%@page import="Model.Aplicacion"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HttpSession sesion = request.getSession(false);
    if (sesion.getAttribute("User") != null) {
        response.sendRedirect("logued.jsp");
    }
    
    ConsultaBD conBD = new ConsultaBD();
    ArrayList<Aplicacion> apps = new ArrayList<Aplicacion>();
    Aplicacion refererApp = null;
    apps = conBD.getAplicaciones();
    
    //Comprovamos si el cliente tiene la cookie de recordar credenciales.
    Cookie[] allCookies = request.getCookies();
    boolean hayCookie = false;
        if (allCookies != null) {
            for (Cookie cookie : allCookies) {
                if (cookie.getName().equals("glrp")) {
                    hayCookie = true;
                    response.setHeader("Refresh", "0;url=ValidarLogin");
                }
                if (sesion.getAttribute("Referer") != null) {
                    if (!(sesion.getAttribute("Referer").toString()).contains("PrimeraVersion")) {
                        //Reiniciamos el valor de sesion de Referer
                        String referer = (sesion.getAttribute("Referer")).toString();
                        for (int i = 0; i < apps.size(); i++) {
                            if (referer.contains(apps.get(i).getUrl())) {
                                refererApp = apps.get(i);
                            }
                        }
                    }
                }
                if (refererApp!=null){
                if (cookie.getName().contains("bloc_") && cookie.getValue().contains(refererApp.getNombre())) {
                    hayCookie = true;
                    %>
                    <html>
                        <head>
                            <%@include file="/WEB-INF/jspf/head.jspf" %>
                            <link rel="stylesheet" type="text/css" href="./css/login.css">
                        </head>
                            <body onload="alert('Has sido bloqueado hasta el <%=sesion.getAttribute("blocHasta")%>');">
                            </body>

                    </html>
                    <%
                    response.setHeader("Refresh", "0;url=login.jsp");
                }
            }
            }
        }
if (!hayCookie){
%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <link rel="stylesheet" type="text/css" href="./css/login.css">
    </head>
    <body>
        <div id="general">
        <%@include file="/WEB-INF/jspf/header.jspf" %>
            <div id=cuerpo>
		<div id="box">
                    <img id="imgBox" border="0" src="./imagenes/mundo.png" alt="imgBox" >
		</div>
                <div id="formLogin">
                    <div class="ribbon">
                            <div class="theribbon">Login</div>
                    </div>
                    <div id="formDiv">
                        <form name="loginForm" action="ValidarLogin" method="POST">
                            <input id="inputLogin" type="text" name="username" placeholder="Usuario" autofocus required><br />
                            <input id="inputLogin" type='password' name='password' value='' placeholder="Contraseña" required><br />
                            <input type="checkbox" name="recordar" value="si" checked>¿Recodar Credenciales?<br />
                            <input type="submit" class="submitButton" value="Iniciar sesión" />
                        </form> 
                        </br>
                        <a href="forgot.jsp">¿Tienes problemas para iniciar sesión?</a></br>
                        <a href="#" onClick="getHintAlert();">Hint</a></br></br>
                        <%if (sesion.getAttribute("Register")!=null){
                            if (Boolean.parseBoolean((String)sesion.getAttribute("Register"))){
                                
                            
                        %>
                        <p>¿No dispones de una cuenta Global Login?</p><a href="register.jsp">¡Regístrate ahora!</a></br>
                        <%}}%>
                        
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<%}%>
