/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

/**
 *
 * @author WiSaM
 */
public class User {
    String name = null;
    String perfil = null;
    String email = null;
    String hint = null;
    String app = null;
    String perfilExt = null;
    

    public User(String name, String perfil) {
        this.name = name;
        this.perfil = perfil;
    }
    
    public User(String name, String perfil, String email, String app, String perfilExt) {
        this.name = name;
        this.perfil = perfil;
        this.email = email;
        this.app = app;
        this.perfilExt = perfilExt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPerfil() {
        switch (Integer.parseInt(perfil)){
            case 1: return "Free";
            case 2: return "Premium";
            case 4: return "Aplicación";
            case 15: return "Admin";
            default: return "0";
        }
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }    

    public String getEmail() {
        return email;
    }

    public String getHint() {
        return hint;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }    

    public String getPerfilExt() {
        return perfilExt;
    }

    public void setPerfilExt(String perfilExt) {
        this.perfilExt = perfilExt;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }
    
}
