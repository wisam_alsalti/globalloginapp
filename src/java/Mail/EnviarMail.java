/*
 * http://www.mkyong.com/java/javamail-api-sending-email-via-gmail-smtp-example/
 */
package Mail;

import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author WiSaM
 */
public class EnviarMail extends Thread{

    final String mailEmisor = "global.login.mail@gmail.com";
    final String password = "passlogin";
    final String server = "smtp.gmail.com";
    final String port = "465";
    String mailReceptor = null;
    String asunto = null;
    String cuerpo = null;

    public EnviarMail(String mailReceptor, String asunto, String cuerpo) {
        this.mailReceptor = mailReceptor;
        this.asunto = asunto;
        this.cuerpo = cuerpo;
    }
    
    @Override
    public void run() {
        Properties props = new Properties();
        props.put("mail.smtp.user", mailEmisor);
        props.put("mail.smtp.host", server);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.socketFactory.port", port);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");

        SecurityManager security = System.getSecurityManager();
        try {
            Authenticator auth = new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(mailEmisor, password);
                }
            };
            Session session = Session.getInstance(props, auth);
            // session.setDebug(true);

            MimeMessage msg = new MimeMessage(session);
            msg.setText(cuerpo);
            msg.setSubject(asunto);
            msg.setFrom(new InternetAddress(mailEmisor));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(mailReceptor));
            Transport.send(msg);

        } catch (MessagingException mex) {
            throw new RuntimeException(mex);

        }
    }

    public String getMailReceptor() {
        return mailReceptor;
    }

    public void setMailReceptor(String mailReceptor) {
        this.mailReceptor = mailReceptor;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

}
