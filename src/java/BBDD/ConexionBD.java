/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BBDD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author WiSaM
 */
public class ConexionBD {
    public String bd = "globallogin";
    public String login = "root";
    public String pass = "toor";
    public String url = "jdbc:mysql://localhost/"+bd;
    
    Connection con = null;
    private static ConexionBD instance;
    
    private ConexionBD(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, login, pass);
            if (con!=null){
                Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, "Conexión a BBDD " + bd + " realizada correctamente.");
            }
            
        }catch ( SQLException | ClassNotFoundException e){
            System.out.println(e);
        }
    }

    /**
     *
     * @return instance
     */
    public static ConexionBD getInstance() {
        if (instance == null){
            instance = new ConexionBD();
        }
        return instance;
    }

    public Connection getCon() {
        return con;
    }
    
    public void desconectar (){
        con = null;
    }
}
