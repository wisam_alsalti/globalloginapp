/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BBDD;

import Model.Aplicacion;
import Model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WiSaM
 */
public class ConsultaBD {

    public ConexionBD conBD;

    public ConsultaBD() {
        this.conBD = ConexionBD.getInstance();
    }

    public int comprovarLogin(String name, String pass) {
        try {
            PreparedStatement pstm = this.conBD.getCon().prepareStatement("SELECT * FROM usuarios WHERE username = ? AND password = ?");
            pstm.setString(1, name);
            pstm.setString(2, pass);
            ResultSet res = pstm.executeQuery();
            if (res.next()) {
                PreparedStatement pstm1 = this.conBD.getCon().prepareStatement("SELECT * FROM usuarios WHERE username = ? AND password = ? AND confirmCode = 'OK'");
                pstm1.setString(1, name);
                pstm1.setString(2, pass);
                ResultSet res1 = pstm1.executeQuery();
                if (res1.next()) {
                    System.out.println("logueado correctamente");
                    return 1;
                } else {
                    System.out.println("email no confirmado");
                    return 2;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 3;
    }

    public int comprovarUserRegister(String name, String email) {
        try {
            PreparedStatement pstm = this.conBD.getCon().prepareStatement("SELECT count(*) as total FROM usuarios WHERE username = ?");
            pstm.setString(1, name);
            ResultSet res = pstm.executeQuery();
            res.next();
            if (0 == res.getInt("total")) {
                pstm = this.conBD.getCon().prepareStatement("SELECT count(*) as total FROM usuarios WHERE email = ?");
                pstm.setString(1, email);
                res = pstm.executeQuery();
                res.next();
                if (0 != res.getInt("total")) {
                    System.out.println("Registro válido");
                    return 2;
                }
            } else {
                System.out.println("Registro NO válido");
                return 1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public boolean bloquearUsuarioAplicacion(String username, String app, int minutos) {
        try {
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.MINUTE, minutos);
            Timestamp bloquearHasta = new Timestamp(cal.getTimeInMillis());

            int aplicacion = 0;
            int user = 0;

            String sql = "SELECT idUsuario FROM usuarios WHERE username = ?";
            PreparedStatement pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, username);
            ResultSet res = pstm.executeQuery();
            while (res.next()) {
                user = res.getInt("idUsuario");
            }

            sql = "SELECT idAplicaciones FROM aplicaciones WHERE nombre = ?";
            pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, app);
            res = pstm.executeQuery();
            while (res.next()) {
                aplicacion = res.getInt("idAplicaciones");
            }

            if (aplicacion == 0 || user == 0) {
                return false;
            } else {
                pstm = this.conBD.getCon().prepareStatement("update usuario_aplicacion set bloqueadoHasta=? where idUsuario = ? and idAplicacion = ?;");
                pstm.setTimestamp(1, bloquearHasta);
                pstm.setInt(2, user);
                pstm.setInt(3, aplicacion);
                if (pstm.executeUpdate() != 0) {
                    return true;
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public int minutosUsuarioAplicacionBloqueado(String username, String app) {
        try {
            Date now = new Date();
            Date bloqueadoHasta = null;
            int aplicacion = 0;
            int user = 0;

            String sql = "SELECT idUsuario FROM usuarios WHERE username = ?";
            PreparedStatement pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, username);
            ResultSet res = pstm.executeQuery();
            while (res.next()) {
                user = res.getInt("idUsuario");
            }

            sql = "SELECT idAplicaciones FROM aplicaciones WHERE nombre = ?";
            pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, app);
            res = pstm.executeQuery();
            while (res.next()) {
                aplicacion = res.getInt("idAplicaciones");
            }

            if (aplicacion == 0 || user == 0) {
                return -1;
            } else {

                pstm = this.conBD.getCon().prepareStatement("select bloqueadoHasta from usuario_aplicacion where idUsuario = ? and idAplicacion = ?;");
                pstm.setInt(1, user);
                pstm.setInt(2, aplicacion);
                res = pstm.executeQuery();
                while (res.next()) {
                    bloqueadoHasta = res.getTimestamp("bloqueadoHasta");
                }
                if (bloqueadoHasta != null) {
                    if (bloqueadoHasta.after(now)) {
                        //retorna la dferencia en minutos entre las dos fechas - segundos que le queda al usuario bloqueado
                        return (int) ((bloqueadoHasta.getTime() - now.getTime()) / 1000);
                    } else {
                        return 0;
                    }
                } else {
                    //el usuario no esta suscrito a la aplicacion.
                    return 0;
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }

        return 0;
    }

    public boolean suscribirUsuarioAplicacion(String username, String app) {
        try {
            int aplicacion = 0;
            int user = 0;

            String sql = "SELECT idUsuario FROM usuarios WHERE username = ?";
            PreparedStatement pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, username);
            ResultSet res = pstm.executeQuery();
            while (res.next()) {
                user = res.getInt("idUsuario");
            }

            sql = "SELECT idAplicaciones FROM aplicaciones WHERE nombre = ?";
            pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, app);
            res = pstm.executeQuery();
            while (res.next()) {
                aplicacion = res.getInt("idAplicaciones");
            }

            if (aplicacion == 0 || user == 0) {
                return false;
            }

            pstm = this.conBD.getCon().prepareStatement("insert into usuario_aplicacion (idUsuario, idAplicacion, externalPerfil) values (?, ?, ?)");
            pstm.setInt(1, user);
            pstm.setInt(2, aplicacion);
            pstm.setInt(3, 1);
            pstm.executeUpdate();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean eliminarUsuario(String username) {
        try {
            int user = 0;

            String sql = "SELECT idUsuario FROM usuarios WHERE username = ?";
            PreparedStatement pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, username);
            ResultSet res = pstm.executeQuery();
            while (res.next()) {
                user = res.getInt("idUsuario");
            }

            if (user == 0) {
                return false;
            }

            ArrayList<Aplicacion> apps = getAplicacionesByNombreUsuario(username);
            //Eliminar suscripciones
            for (int i = 0; i < apps.size(); i++) {
                eliminarSuscripcion(username, apps.get(i).getNombre());
            }

            pstm = this.conBD.getCon().prepareStatement("delete from usuarios where idUsuario=?");
            pstm.setInt(1, user);
            pstm.executeUpdate();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean eliminarSuscripcion(String username, String app) {
        try {
            int aplicacion = 0;
            int user = 0;

            String sql = "SELECT idUsuario FROM usuarios WHERE username = ?";
            PreparedStatement pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, username);
            ResultSet res = pstm.executeQuery();
            while (res.next()) {
                user = res.getInt("idUsuario");
            }

            sql = "SELECT idAplicaciones FROM aplicaciones WHERE nombre = ?";
            pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, app);
            res = pstm.executeQuery();
            while (res.next()) {
                aplicacion = res.getInt("idAplicaciones");
            }

            if (aplicacion == 0 || user == 0) {
                return false;
            }

            pstm = this.conBD.getCon().prepareStatement("delete from usuario_aplicacion where idUsuario=? and idAplicacion=?");
            pstm.setInt(1, user);
            pstm.setInt(2, aplicacion);
            pstm.executeUpdate();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean nuevoRegistro(String name, String password, String email, String confirmCode, String hint) {
        try {
            PreparedStatement pstm = this.conBD.getCon().prepareStatement("INSERT INTO usuarios (username, password, email, confirmCode, hint) VALUES (?, ?, ?, ?, ?)");
            pstm.setString(1, name);
            pstm.setString(2, password);
            pstm.setString(3, email);
            pstm.setString(4, confirmCode);
            pstm.setString(5, hint);
            pstm.executeUpdate();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean confirmarRegistro(String code) {
        try {
            String sql = "SELECT idUsuario FROM usuarios WHERE confirmCode = ?";
            PreparedStatement pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, code);
            ResultSet res = pstm.executeQuery();
            while (res.next()) {
                String ok = "OK";
                sql = "UPDATE usuarios SET confirmCode = ? WHERE idUsuario = ?";
                PreparedStatement stm = this.conBD.getCon().prepareStatement(sql);
                stm.setString(1, ok);
                stm.setInt(2, res.getInt("idUsuario"));
                stm.executeUpdate();
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public ArrayList<Aplicacion> getAplicacionesByNombreUsuario(String codiUsuario) {
        ArrayList<Aplicacion> lista = new ArrayList();
        try {
            String sql = "SELECT * FROM globallogin.aplicaciones where idAplicaciones in "
                    + "(SELECT idAplicacion FROM globallogin.usuario_aplicacion where idUsuario in "
                    + "(SELECT idUsuario from usuarios where username = ?));";
            PreparedStatement pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, codiUsuario);
            ResultSet res = pstm.executeQuery();
            while (res.next()) {
                Aplicacion aplicacion = new Aplicacion(res.getString("nombre"), res.getString("url"));
                lista.add(aplicacion);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    public ArrayList<Aplicacion> getAplicaciones() {
        ArrayList<Aplicacion> lista = new ArrayList();
        try {
            String sql = "SELECT * FROM aplicaciones where idAplicaciones in "
                    + "(SELECT idAplicacion from usuario_aplicacion, usuarios);";
            PreparedStatement pstm = this.conBD.getCon().prepareStatement(sql);
            ResultSet res = pstm.executeQuery();
            while (res.next()) {
                Aplicacion aplicacion = new Aplicacion(res.getString("nombre"), res.getString("url"));
                lista.add(aplicacion);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    public String getUserByEmail(String email) {
        String ret = null;
        try {
            String sql = "SELECT * FROM usuarios WHERE email = ?;";
            PreparedStatement pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, email);
            ResultSet res = pstm.executeQuery();
            if (res.next()) {
                ret = res.getString("username");
            }

        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

    public String getUserByRememberPassCode(String code) {
        String ret = null;
        try {
            String sql = "SELECT * FROM usuarios WHERE rememberPassCode = ?;";
            PreparedStatement pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, code);
            ResultSet res = pstm.executeQuery();
            if (res.next()) {
                ret = res.getString("username");
            }

        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

    public String getHintByUser(String username) {
        String ret = null;
        try {
            String sql = "SELECT * FROM usuarios WHERE username = ?;";
            PreparedStatement pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, username);
            ResultSet res = pstm.executeQuery();
            if (res.next()) {
                ret = res.getString("hint");
            }

        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

    public int getInternalPerfilByUser(String username) {
        int ret = 0;
        try {
            String sql = "SELECT internalPerfil FROM usuarios WHERE username = ?;";
            PreparedStatement pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, username);
            ResultSet res = pstm.executeQuery();
            if (res.next()) {
                ret = res.getInt("internalPerfil");
                if (ret > 15) {
                    ret = 0;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

    public int getExternalPerfilByUser(String username) {
        int ret = 0;
        try {
            String sql = "SELECT idUsuario FROM usuarios WHERE username = ?;";
            PreparedStatement pstm = this.conBD.getCon().prepareStatement(sql);
            pstm.setString(1, username);
            ResultSet res = pstm.executeQuery();
            if (res.next()) {
                sql = "SELECT externalPerfil FROM usuario_aplicacion WHERE idUsuario = ?;";
                pstm = this.conBD.getCon().prepareStatement(sql);
                pstm.setInt(1, res.getInt("idUsuario"));
                res = pstm.executeQuery();
                if (res.next()) {
                    ret = res.getInt("externalPerfil");
                    //Como maximo 15 perfiles externos
                    if (ret > 15) {
                        ret = 0;
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

    public boolean putHintByUser(String user, String hint) {
        try {
            PreparedStatement pstm = this.conBD.getCon().prepareStatement("UPDATE usuarios SET hint = ? WHERE username = ?;");
            pstm.setString(1, hint);
            pstm.setString(2, user);
            pstm.executeUpdate();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean putCodePass(String email, String codePass) {
        try {
            PreparedStatement pstm = this.conBD.getCon().prepareStatement("UPDATE usuarios SET cambioPassCode = ? WHERE email = ?;");
            pstm.setString(1, codePass);
            pstm.setString(2, email);
            pstm.executeUpdate();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean putCodeRemember(String user, String rememberCode) {
        try {
            PreparedStatement pstm = this.conBD.getCon().prepareStatement("UPDATE usuarios SET rememberPassCode = ? WHERE username = ?;");
            pstm.setString(1, rememberCode);
            pstm.setString(2, user);
            pstm.executeUpdate();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean cambiarPass(String codePass, String user, String pass) {
        String username = user;
        try {
            if (codePass != null) {
                PreparedStatement pstm = this.conBD.getCon().prepareStatement("SELECT username FROM usuarios WHERE cambioPassCode = ?;");
                pstm.setString(1, codePass);
                ResultSet res = pstm.executeQuery();
                if (res.next()) {
                    username = res.getString("username");
                } else {
                    return false;
                }
            }

            if (username != null) {
                String ok = "OK";
                PreparedStatement stm = this.conBD.getCon().prepareStatement("UPDATE usuarios SET cambioPassCode = ?, password = ? WHERE username = ?");
                stm.setString(1, ok);
                stm.setString(2, pass);
                stm.setString(3, username);
                stm.executeUpdate();
            }
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public int setUserByAdmin(String oldUserName, String oldEmail, String name, String email, String perfI, String perfE, String app) {
        boolean aux = false; //booleano de control interno del metodo
        if ((comprovarUserRegister(name, oldEmail) != 1) || (oldUserName.equals(name))) {

            try {
                if (oldUserName != null) {
                    PreparedStatement pstm = this.conBD.getCon().prepareStatement("select idUsuario from usuarios where username = ?");
                    pstm.setString(1, oldUserName);
                    ResultSet res = pstm.executeQuery();
                    if (res.next()) {
                        int idUser = res.getInt("idUsuario");

                        //Modificar Nombre, email y perfilInterno
                        String sql = "update usuarios set";
                        if (name != null && !name.isEmpty() && !oldUserName.equals(name)) {
                            sql += " username='" + name + "'";
                            aux = true;
                        }

                        if (email != null && !email.isEmpty() && !oldEmail.equals(email)) {
                            if (aux) {
                                sql += ",";
                            }
                            sql += " email='" + email + "'";
                            aux = true;
                        }

                        if (perfI != null && !perfI.isEmpty()) {
                            if (aux) {
                                sql += ",";
                            }
                            sql += " internalPerfil='" + Integer.parseInt(perfI) + "'";
                            aux = true;
                        }
                        sql += " where idUsuario =?;";
                        if (aux) {
                            PreparedStatement pstma = this.conBD.getCon().prepareStatement(sql);
                            pstma.setInt(1, idUser);
                            pstma.executeUpdate();
                        }
                        //FIN Modificar Nombre, email y perfilInterno

                        //Modificar perfil externo
                        if (perfE != null && !perfE.isEmpty()) {
                            pstm = this.conBD.getCon().prepareStatement("update usuario_aplicacion join aplicaciones as app on usuario_aplicacion.idAplicacion=app.idAplicaciones set externalPerfil=? where usuario_aplicacion.idUsuario = ? and app.nombre=?;");
                            pstm.setInt(1, Integer.parseInt(perfE));
                            pstm.setInt(2, idUser);
                            pstm.setString(3, app);
                            pstm.executeUpdate();
                        }
                        //FIN Modificar perfil externo
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            //Nombre de usuario ya existe..
            return 1;
        }
        return 0;
    }

    public ArrayList getUserListByCriteria(String username, String aplicacion, String perfilInterno, String perfilExterno) {
        int i = 1;
        ArrayList<User> ret = new ArrayList<User>();
        String sql = "select username, email, internalPerfil, nombre, externalPerfil from usuarios left outer join usuario_aplicacion on usuarios.idUsuario = usuario_aplicacion.idUsuario left outer join aplicaciones on aplicaciones.idAplicaciones = usuario_aplicacion.idAplicacion";

        if (!username.equals("")) {
            sql += " where username = ?";
        }

        if (!aplicacion.equals("")) {
            if (!sql.contains("where")) {
                sql += " where";
            } else {
                sql += " and";
            }
            sql += " nombre = ?";
        }

        if (!perfilInterno.equals("")) {
            if (!sql.contains("where")) {
                sql += " where";
            } else {
                sql += " and";
            }
            sql += " internalPerfil = ?";
        }

        if (!perfilExterno.equals("")) {
            if (!sql.contains("where")) {
                sql += " where";
            } else {
                sql += " and";
            }
            sql += " externalPerfil = ?";
        }
        sql += ";";

        try {
            PreparedStatement pstm = this.conBD.getCon().prepareStatement(sql);
            if (!username.equals("")) {
                pstm.setString(i, username);
                i++;
            }
            if (!aplicacion.equals("")) {
                pstm.setString(i, aplicacion);
                i++;
            }
            if (!perfilInterno.equals("")) {
                pstm.setInt(i, Integer.parseInt(perfilInterno));
                i++;
            }
            if (!perfilExterno.equals("")) {
                pstm.setInt(i, Integer.parseInt(perfilExterno));
                i++;
            }

            ResultSet res = pstm.executeQuery();
            while (res.next()) {
                User user = new User(res.getString("username"), Integer.toString(res.getInt("internalPerfil")), res.getString("email"), res.getString("nombre"), Integer.toString(res.getInt("externalPerfil")));
                ret.add(user);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ConsultaBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

}
