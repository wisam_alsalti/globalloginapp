/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 * SaveKeyPair y LoadKeyPair  --> adaptadas y extraidas de http://snipplr.com/view/18368/
 *
 */
package Token;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WiSaM
 */
public class RSAGenerator {
    static final String KEYS_PATH = "C:\\Users\\WiSaM\\Desktop\\TFG\\RSAKeys";
    
    KeyPair kp;
    PrivateKey privateKey;
    PublicKey publicKey;

    public RSAGenerator() {
        // Comprobamos si en KEYS_PATH existen los ficheros con las llaves
        kp = LoadKeyPair(KEYS_PATH, "RSA");
        if (kp == null) {
            // Generamos nuevas public/private keys y las guardamos en KEYS_PATH
            try {
                KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
                keyGenerator.initialize(2048);
                kp = keyGenerator.genKeyPair();
                privateKey = kp.getPrivate();
                publicKey = kp.getPublic();
                SaveKeyPair(KEYS_PATH, kp);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(RSAGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public KeyPair getKp() {
        return kp;
    }

    public void setKp(KeyPair kp) {
        this.kp = kp;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }
    
    private void SaveKeyPair(String path, KeyPair keyPair) {
        FileOutputStream fos = null;
        try {
            privateKey = keyPair.getPrivate();
            publicKey = keyPair.getPublic();

            // Store Public Key.
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(publicKey.getEncoded());
            fos = new FileOutputStream(path + "/public.key");
            fos.write(x509EncodedKeySpec.getEncoded());
            fos.close();

            // Store Private Key.
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(privateKey.getEncoded());
            fos = new FileOutputStream(path + "/private.key");
            fos.write(pkcs8EncodedKeySpec.getEncoded());
            fos.close();

        } catch (IOException ex) {
            System.out.println("ERROR: Fallo al intentar almacenar las public/private keys.");
            Logger.getLogger(RSAGenerator.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(RSAGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private KeyPair LoadKeyPair(String path, String algorithm) {
        FileInputStream fis = null;
        KeyPair ret = null;
        try {
            // Read Public Key.
            File filePublicKey = new File(path + "/public.key");
            fis = new FileInputStream(path + "/public.key");
            byte[] encodedPublicKey = new byte[(int) filePublicKey.length()];
            fis.read(encodedPublicKey);
            fis.close();

            // Read Private Key.
            File filePrivateKey = new File(path + "/private.key");
            fis = new FileInputStream(path + "/private.key");
            byte[] encodedPrivateKey = new byte[(int) filePrivateKey.length()];
            fis.read(encodedPrivateKey);
            fis.close();

            // Generate KeyPair.
            KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encodedPublicKey);
            publicKey = keyFactory.generatePublic(publicKeySpec);
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey);
            privateKey = keyFactory.generatePrivate(privateKeySpec);
            ret = new KeyPair(publicKey, privateKey);

        } catch (IOException ex) {
            System.out.println("ERROR: No se ha podido acceder a los ficheros o la ruta es incorrecta.");
            Logger.getLogger(RSAGenerator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            System.out.println("ERROR: El algoritmo pasado por parámetro no es correcto.");
            Logger.getLogger(RSAGenerator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            System.out.println("ERROR: Fallo al intentar leer las private/public keys.");
            Logger.getLogger(RSAGenerator.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(RSAGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return ret;
    }
}
