/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Token;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;

/**
 *
 * @author WiSaM
 */
public class MyToken {

    public static void main(final String[] args) throws NoSuchAlgorithmException, JOSEException, ParseException {

        boolean proceso = true;

        // RSA signatures require a public and private RSA key pair,
        // the public key must be made known to the JWS recipient in
        // order to verify the signatures
        KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
        keyGenerator.initialize(2048);

        KeyPair kp = keyGenerator.genKeyPair();
        RSAPublicKey publicKey = (RSAPublicKey) kp.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) kp.getPrivate();

        System.out.println("-----PublicKey-----");
        System.out.println(publicKey.toString());
        System.out.println("-----PrivateKey-----");
        System.out.println(privateKey.toString());

        // Create RSA-signer with the private key
        JWSSigner signer = new RSASSASigner(privateKey);

        System.out.println("-----Signer-----");
        System.out.println(signer.toString());

        // Prepare JWS object with simple string as payload
        JWSObject jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.RS256), new Payload("In RSA we trust!"));
        
        System.out.println(jwsObject.getHeader().toString());
        System.out.println(jwsObject.getPayload().toString());

        // Compute the RSA signature
        jwsObject.sign(signer);

        System.out.println("-----jwsObject.Sign-----");
        if (jwsObject.getState().equals(JWSObject.State.SIGNED)) {
            System.out.println("(jwsObject.getState == SIGNED) --> True");
        } else {
            proceso = false;
            System.out.println("(jwsObject.getState == SIGNED) --> False");
        }

        // To serialize to compact form, produces something like
        // eyJhbGciOiJSUzI1NiJ9.SW4gUlNBIHdlIHRydXN0IQ.IRMQENi4nJyp4er2L
        // mZq3ivwoAjqa1uUkSBKFIX7ATndFF5ivnt-m8uApHO4kfIFOrW7w2Ezmlg3Qd
        // maXlS9DhN0nUk_hGI3amEjkKd0BWYCB8vfUbUv0XGjQip78AI4z1PrFRNidm7
        // -jPDm5Iq0SZnjKjCNS5Q15fokXZc8u0A
        System.out.println("-----jwsObject-----");
        System.out.println(jwsObject.serialize());

        String jwtString = jwsObject.serialize();
        String redirectUrl = "http://localhost:8080/Aplicacion1/index.jsp/jwt?jwt=" + jwtString;
        
        //response.sendRedirect(redirectUrl);
        
        
        
        
        
        
        //********************CLIENTE***************************

        // To parse the JWS and verify it, e.g. on client-side
        jwsObject = JWSObject.parse(jwtString);

        //PRUEBA --> generar error creando otra llave publica
//        kp = keyGenerator.genKeyPair();
//        publicKey = (RSAPublicKey) kp.getPublic();
        JWSVerifier verifier = new RSASSAVerifier(publicKey);

        System.out.println("-----jwsObject.verify-----");
        if (jwsObject.verify(verifier)) {
            System.out.println("jwsObject.verify --> True");
        } else {
            proceso = false;
            System.out.println("jwsObject.verify --> False");
        }

        System.out.println("-----Message-----");
        if (jwsObject.getPayload().toString().equals("In RSA we trust!")) {
            System.out.println("jwsObject.getPayload --> True");
        } else {
            proceso = false;
            System.out.println("jwsObject.getPayload --> False");
        }

        System.out.println("---------------------");
        if (proceso == true) {
            System.out.println("Proceso finalizado correctamente.");
        } else {
            System.out.println("Error en algun paso.");
        }
    }

}

