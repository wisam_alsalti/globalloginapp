/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Token;

import Model.User;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @author WiSaM
 */
public class TokenGenerator {
    public String takeToken(User user) throws JOSEException{
        RSAGenerator rsaGenerator = new RSAGenerator();
        KeyPair rsaKeys = rsaGenerator.getKp();
        
        // Create RSA-signer with the private key
        JWSSigner signer = new RSASSASigner((RSAPrivateKey)rsaKeys.getPrivate());
        
        // MockUser simula objeto recogido de BBDD
        
        JWTClaimsSet jwtClaims = new JWTClaimsSet();
        jwtClaims.setIssueTime(new Date());
        //jwtClaims.setExpirationTime();
        jwtClaims.setJWTID(UUID.randomUUID().toString());
        jwtClaims.setClaim("nom", user.getName());
        jwtClaims.setClaim("prf", user.getPerfilExt());

        System.out.println("payload --> " + jwtClaims.toJSONObject());
        Payload payload = new Payload(jwtClaims.toJSONObject());
        
        
        JWSHeader header = new JWSHeader(JWSAlgorithm.RS256);
        header.setContentType("text/plain");
        System.out.println("header --> " + header.toJSONObject());
        
        // Creamos el objeto y firmamos
        JWSObject jwsObject = new JWSObject(header, payload);
        jwsObject.sign(signer);
        
        // Serialise to JWT compact form
        String jwtString = jwsObject.serialize();

        return jwtString;
    }
    
    
//    public static void main(final String[] args) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, JOSEException{
//        RSAGenerator rsaGenerator = new RSAGenerator();
//        KeyPair rsaKeys = rsaGenerator.getKp();
//        
//        // Create RSA-signer with the private key
//        JWSSigner signer = new RSASSASigner((RSAPrivateKey)rsaKeys.getPrivate());
//        
//        // MockUser simula objeto recogido de BBDD
//        String nom = "Pepe";
//        String perfil = "15";
//        User user = new User(nom, perfil);
//        
//        JWTClaimsSet jwtClaims = new JWTClaimsSet();
//        jwtClaims.setIssueTime(new Date());
//        //jwtClaims.setExpirationTime();
//        jwtClaims.setJWTID(UUID.randomUUID().toString());
//        jwtClaims.setClaim("nom", user.getName());
//        jwtClaims.setClaim("pfr", user.getPerfil());
//
//        System.out.println("payload --> " + jwtClaims.toJSONObject());
//        Payload payload = new Payload(jwtClaims.toJSONObject());
//        
//        
//        JWSHeader header = new JWSHeader(JWSAlgorithm.RS256);
//        header.setContentType("text/plain");
//        System.out.println("header --> " + header.toJSONObject());
//        
//        // Creamos el objeto y firmamos
//        JWSObject jwsObject = new JWSObject(header, payload);
//        jwsObject.sign(signer);
//        
//        // Serialise to JWT compact form
//        String jwtString = jwsObject.serialize();
//        
//        
//        
//        
//        
//    }
    
    
    
}
