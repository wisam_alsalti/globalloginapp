/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Token;

import com.google.gson.JsonObject;
import java.security.InvalidKeyException;
import java.security.SignatureException;
import java.util.Calendar;
import net.oauth.jsontoken.JsonToken;
import static net.oauth.jsontoken.JsonToken.ISSUER;
import net.oauth.jsontoken.crypto.HmacSHA256Signer;
import org.joda.time.Instant;

public class JsonWebToken {

    public JsonWebToken() {
    
    }
    
    private final String SIGNING_KEY = "Hola";

    public String getJWT() throws InvalidKeyException, SignatureException {
        JsonToken token = null;
        token = createToken();
        return token.serializeAndSign();
    }

    private JsonToken createToken() throws InvalidKeyException {
        //Current time and signing algorithm
        Calendar cal = Calendar.getInstance();
        HmacSHA256Signer signer = new HmacSHA256Signer(ISSUER, null, SIGNING_KEY.getBytes());
        
        System.out.println(signer.sign(SIGNING_KEY.getBytes()));

        //Configure JSON token
        JsonToken token = new JsonToken(signer);
        token.setAudience("Google");
        token.setParam("typ", "google/payments/inapp/item/v1");
        token.setIssuedAt(new Instant(cal.getTimeInMillis()));
        token.setExpiration(new Instant(cal.getTimeInMillis() + 60000L));
System.out.println(signer.toString());
        //Configure request object
        JsonObject request = new JsonObject();
        request.addProperty("name", "Piece of Cake");
        request.addProperty("description", "Virtual chocolate cake to fill your virtual tummy");
        request.addProperty("price", "10.50");
        request.addProperty("currencyCode", "USD");
        request.addProperty("sellerData", "user_id:1224245,offer_code:3098576987,affiliate:aksdfbovu9j");
System.out.println(signer.toString());
        JsonObject payload = token.getPayloadAsJsonObject();
        payload.add("request", request);
System.out.println(token.toString());
System.out.println("---> " + payload.getAsString());
        return token;
    }
}
