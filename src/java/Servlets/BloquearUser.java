/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import BBDD.ConsultaBD;
import Model.Aplicacion;
import Model.User;
import Utils.Util;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author WiSaM
 */
public class BloquearUser extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DeleteUser</title>");
            out.println("</head>");
            out.println("<body>");
            Util utilb = new Util();
            HttpSession sesion = request.getSession(false);
            String user = "";
            String app = "";
            String minuts = "";

            if (sesion.getAttribute("User") == null) {
                response.sendRedirect("login.jsp");
            } else if (!utilb.isAdmin((String) sesion.getAttribute("User"))) {
                response.sendRedirect("logued.jsp");
            } else {

                user = (request.getParameter("user") != null) ? request.getParameter("user") : "";
                app = (request.getParameter("app") != null) ? request.getParameter("app") : "";
                minuts = (request.getParameter("minuts") != null) ? request.getParameter("minuts") : "";

                ConsultaBD conBD = new ConsultaBD();
                
                if (minuts.equals("") || app.equals("") || user.equals("")){
                    String msg = "Problemas a la hora de bloquear el usuario.";
                    out.println("<body onload=\"alert('" + msg + "');\">");
                    response.setHeader("Refresh", "0;url=cpanel.jsp");
                }else{
                    conBD.bloquearUsuarioAplicacion(user, app, Integer.valueOf(minuts));
                    String msg = "Suscripción bloqueada correctamente.";
                    out.println("<body onload=\"alert('" + msg + "');\">");
                    response.setHeader("Refresh", "0;url=cpanel.jsp");
                }
            }
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
