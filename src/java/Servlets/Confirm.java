/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Servlets;

import BBDD.ConsultaBD;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author WiSaM
 */
public class Confirm extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>\n");
            out.println("<html>\n");
            out.println("    <head>\n");
            out.println("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
            out.println("        <title>Confirmar Email</title>\n");
            out.println("    </head>\n");
            out.println("    <body>\n");
            out.println("        ");
            
            String confirmCode = request.getParameter("confirmCode");
            if (confirmCode!=null){
                if (!confirmCode.equals("OK")) {
                    ConsultaBD conBD = new ConsultaBD();
                    if (conBD.confirmarRegistro(confirmCode)) {
                        String msg = "Email confirmado correctamente.. ";
                        out.print("<body onload=\"alert('" + msg + "');\">");
                        response.setHeader("Refresh", "0;url=login.jsp");
                    } else {
                        out.println("<p>Fallo al confirmar su email, puede ponerse en contacto con nosotros en "
                                + "<a href=\"mailto:global.login.mail@gmail.com\">Global.Login.Mail@gmail.com</a></p>");
                    }
                }
            }else{
                out.println("<p>Fallo al confirmar su email, puede ponerse en contacto con nosotros en "
                            + "<a href=\"mailto:global.login.mail@gmail.com\">Global.Login.Mail@gmail.com</a></p>");
            }
            
            out.println("\n");
            out.println("    </body>\n");
            out.println("</html>\n");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
