/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import BBDD.ConsultaBD;
import Mail.EnviarMail;
import Model.Aplicacion;
import Model.User;
import Token.TokenGenerator;
import Utils.Util;
import com.nimbusds.jose.JOSEException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author WiSaM
 */
public class ValidarLogin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws com.nimbusds.jose.JOSEException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JOSEException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>\n");
            out.println("<html>\n");
            out.println("    <head>\n");
            out.println("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
            out.println("        <title>Validar Login</title>\n");
            out.println("    </head>\n");
            out.println("        ");
            ConsultaBD conBD = new ConsultaBD();
            ArrayList<Aplicacion> apps = new ArrayList<Aplicacion>();
            Aplicacion refererApp = null;
            apps = conBD.getAplicaciones();
            HttpSession sesion = request.getSession(true);
            //Si hay sesión iniciada y hay acceso a este jsp, 
            //guardamos de donde se viene.
            if (sesion.getAttribute("User") != null) {
                if (sesion.getAttribute("Referer") != null) {
                    String referer = sesion.getAttribute("Referer").toString();
                    for (int i = 0; i < apps.size(); i++) {
                        if (referer.contains(apps.get(i).getUrl())) {
                            refererApp = apps.get(i);
                        }
                    }
                    sesion.setAttribute("Referer", null);
                    if (refererApp != null && !referer.contains("PrimeraVersion")) {
                        TokenGenerator tGen = new TokenGenerator();
                        ArrayList<User> userActual = conBD.getUserListByCriteria((String) sesion.getAttribute("User"), refererApp.getNombre(), "", "");
                        if (userActual != null) {
                            if (!userActual.isEmpty()) {
                                String token = tGen.takeToken(userActual.get(0));
                                //Comprobar si el usuario esta bloqueado.
                                int tiempoBloqueo = conBD.minutosUsuarioAplicacionBloqueado(userActual.get(0).getName(), refererApp.getNombre());
                                if (tiempoBloqueo == 0) {
                                    //formato --> http://localhost:8080/Aplicacion3/jwt?jwt=
                                    out.println("<body>");
                                    response.sendRedirect(refererApp.getUrl() + "jwt.jsp?jwt=" + token);
                                } else {
                                    //No usuario bloqueado
                                    Calendar cal = Calendar.getInstance();
                                    cal.setTime(new Date());
                                    cal.add(Calendar.SECOND, tiempoBloqueo);
                                    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                    String msg = "Has sido bloqueado hasta el  " + df.format(cal.getTime());
                                    out.println("<body onload=\"alert('" + msg + "');\">");
                                    response.setHeader("Refresh", "0;url=logued.jsp");
                                }
                            } else {
                                //No suscrito
                                String msg = "No estás suscrito a esta página.";
                                out.println("<body onload=\"alert('" + msg + "');\">");
                                response.setHeader("Refresh", "0;url=logued.jsp");
                            }
                        } else {
                            response.sendRedirect("logued.jsp");
                        }
//
                    } else {
                        response.sendRedirect("logued.jsp");
                    }
                } else if (request.getHeader("Referer") != null) {
                    String referer = request.getHeader("Referer").toString();
                    for (int i = 0; i < apps.size(); i++) {
                        if (referer.contains(apps.get(i).getUrl())) {
                            refererApp = apps.get(i);
                        }
                    }
                    if (refererApp != null && !referer.contains("PrimeraVersion")) {
                        TokenGenerator tGen = new TokenGenerator();
                        ArrayList<User> userActual = conBD.getUserListByCriteria((String) sesion.getAttribute("User"), refererApp.getNombre(), "", "");
                        if (userActual != null) {
                            if (!userActual.isEmpty()) {
                                String token = tGen.takeToken(userActual.get(0));
                                //Comprobar si el usuario esta bloqueado.
                                int tiempoBloqueo = conBD.minutosUsuarioAplicacionBloqueado(userActual.get(0).getName(), refererApp.getNombre());
                                if (tiempoBloqueo == 0) {
                                    //formato --> http://localhost:8080/Aplicacion3/jwt?jwt=
                                    out.println("<body>");
                                    response.sendRedirect(refererApp.getUrl() + "jwt.jsp?jwt=" + token);
                                } else {
                                    //No usuario bloqueado
                                    Calendar cal = Calendar.getInstance();
                                    cal.setTime(new Date());
                                    cal.add(Calendar.SECOND, tiempoBloqueo);
                                    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                                    System.out.println(tiempoBloqueo);
                                    System.out.println((cal.getTime()));
                                    System.out.println(df.format(cal.getTime()));

                                    String msg = "Has sido bloqueado hasta el  " + df.format(cal.getTime());
                                    out.println("<body onload=\"alert('" + msg + "');\">");
                                    response.setHeader("Refresh", "0;url=logued.jsp");
                                }
                            } else {
                                //No suscrito
                                String msg = "No estás suscrito a esta página.";
                                out.println("<body onload=\"alert('" + msg + "');\">");
                                response.setHeader("Refresh", "0;url=logued.jsp");
                            }
                        } else {
                            response.sendRedirect("logued.jsp");
                        }
                    } else {
                        response.sendRedirect("logued.jsp");
                    }

                } else {
                    response.sendRedirect("logued.jsp");
                }

            } //Si no hay sesión iniciada:
            else {
                conBD = new ConsultaBD();
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                String recordar = request.getParameter("recordar");
                if (username != null || password != null) {
                    int resultat = conBD.comprovarLogin(username, DigestUtils.md5Hex(password.trim()));
                    if (1 == resultat) {
                        //Logueado correctamente
                        //Reiniciamos variables de sesion de bloqueo
                        sesion.setAttribute("intentosNombreUser", null);
                        sesion.setAttribute("blocIntentos", sesion.getAttribute("blocIntentosOrig"));
                        sesion = request.getSession(true);
                        sesion.setAttribute("User", username);
                        if (recordar != null) {
                            if (recordar.equals("si")) {
                                Util util = new Util();
                                String rememberCode = util.randomString(15);
                                conBD.putCodeRemember(username, rememberCode);
                                Cookie cookie = new Cookie("glrp", rememberCode);
                                //31 dias..
                                cookie.setMaxAge(60 * 60 * 24 * 31);
                                response.addCookie(cookie);
                            } else {
                                //Destruimos la cookie
                                Cookie[] allCookies = request.getCookies();
                                if (allCookies != null) {
                                    for (Cookie cookie : allCookies) {
                                        if (cookie.getName().equals("glrp")) {
                                            cookie.setMaxAge(0);
                                            response.addCookie(cookie);
                                            if (sesion.getAttribute("User") != null) {

                                                conBD.putCodeRemember(sesion.getAttribute("User").toString(), "NO");
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            //Destruimos la cookie
                            Cookie[] allCookies = request.getCookies();
                            if (allCookies != null) {
                                for (Cookie cookie : allCookies) {
                                    if (cookie.getName().equals("glrp")) {
                                        cookie.setMaxAge(0);
                                        response.addCookie(cookie);
                                        if (sesion.getAttribute("User") != null) {

                                            conBD.putCodeRemember(sesion.getAttribute("User").toString(), "NO");
                                        }
                                    }
                                }
                            }
                        }

                        //Provisional pruebas (optimizable)
                        if (sesion.getAttribute("Referer") != null) {
                            if (!(sesion.getAttribute("Referer").toString()).contains("PrimeraVersion")) {
                                //Reiniciamos el valor de sesion de Referer
                                String referer = (sesion.getAttribute("Referer")).toString();
                                for (int i = 0; i < apps.size(); i++) {
                                    if (referer.contains(apps.get(i).getUrl())) {
                                        refererApp = apps.get(i);
                                    }
                                }
                                sesion.setAttribute("Referer", null);
                                if (refererApp != null && !referer.contains("PrimeraVersion")) {
                                    TokenGenerator tGen = new TokenGenerator();
                                    ArrayList<User> userActual = conBD.getUserListByCriteria((String) sesion.getAttribute("User"), refererApp.getNombre(), "", "");
                                    if (userActual != null) {
                                        if (!userActual.isEmpty()) {
                                            String token = tGen.takeToken(userActual.get(0));
                                            //Comprobar si el usuario esta bloqueado.

                                            int tiempoBloqueo = conBD.minutosUsuarioAplicacionBloqueado(userActual.get(0).getName(), refererApp.getNombre());
                                            if (tiempoBloqueo == 0) {
                                                //formato --> http://localhost:8080/Aplicacion3/jwt?jwt=
                                                out.println("<body>");
                                                response.sendRedirect(refererApp.getUrl() + "jwt.jsp?jwt=" + token);
                                            } else {
                                                //No usuario bloqueado
                                                Calendar cal = Calendar.getInstance();
                                                cal.setTime(new Date());
                                                cal.add(Calendar.SECOND, tiempoBloqueo);
                                                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                                String msg = "Has sido bloqueado hasta el  " + df.format(cal.getTime());
                                                out.println("<body onload=\"alert('" + msg + "');\">");
                                                response.setHeader("Refresh", "0;url=logued.jsp");
                                            }
                                        } else {
                                            //No suscrito
                                            String msg = "No estás suscrito a esta página.";
                                            out.println("<body onload=\"alert('" + msg + "');\">");
                                            response.setHeader("Refresh", "0;url=logued.jsp");

                                        }
                                    } else {
                                        response.sendRedirect("logued.jsp");
                                    }

                                } else {
                                    response.sendRedirect("logued.jsp");
                                }
                            }
                        } else {
                            response.sendRedirect("logued.jsp");
                        }
                    } else if (resultat == 2) {
                        //Email no confirmado
                        String msg = "Email no confirmado.";
                        out.print("<body onload=\"alert('" + msg + "');\">");
                        response.setHeader("Refresh", "0;url=login.jsp");
                    } else {
                        //Credenciales incorrectos
                        if (sesion.getAttribute("Referer") != null) {
                            if (!(sesion.getAttribute("Referer").toString()).contains("PrimeraVersion")) {
                                //Reiniciamos el valor de sesion de Referer
                                String referer = (sesion.getAttribute("Referer")).toString();
                                for (int i = 0; i < apps.size(); i++) {
                                    if (referer.contains(apps.get(i).getUrl())) {
                                        refererApp = apps.get(i);
                                    }
                                }
                            }
                        }
                        if (refererApp != null) {
                            if (sesion.getAttribute("blocMinutos") != null) {
                                //Aplicación con boqueo
                                int blocMinutos = Integer.parseInt(sesion.getAttribute("blocMinutos").toString());
                                if (blocMinutos > 0) {
                                    if (sesion.getAttribute("blocIntentos") != null) {
                                        int blocIntentos = Integer.parseInt(sesion.getAttribute("blocIntentos").toString());
                                        System.out.println("intentos.. " + blocIntentos);
                                        //Variable de sesion con un listado del nombre de usuarios de intento
                                        if (sesion.getAttribute("intentosNombreUser") != null) {
                                            sesion.setAttribute("intentosNombreUser", sesion.getAttribute("intentosNombreUser") + "," + username);
                                        } else {
                                            sesion.setAttribute("intentosNombreUser", username);
                                        }
                                        System.out.println(sesion.getAttribute("intentosNombreUser"));
                                        String[] listaUsersIntentos = sesion.getAttribute("intentosNombreUser").toString().split(",");

                                        if (sesion.getAttribute("blocIntentosUser") != null) {
                                            int blocIntentosUser = Integer.parseInt(sesion.getAttribute("blocIntentosUser").toString());
                                            if (usuario2Bloquear(Arrays.asList(listaUsersIntentos), blocIntentosUser, blocMinutos, refererApp.getNombre())) {
                                                System.out.println("bloqueado bbdd");
                                                blocIntentos = 1;
                                            }
                                        }

                                        //Bloqueamos
                                        if (blocIntentos <= 1) {
                                            sesion.setAttribute("intentosNombreUser", null);
                                            sesion.setAttribute("blocIntentos", sesion.getAttribute("blocIntentosOrig"));
                                            //Bloquear con cookie
                                            System.out.println("bloqueado.. ");
                                            Cookie cookie = new Cookie("bloc_" + refererApp.getNombre(), refererApp.getNombre());
                                            //variable de sesion para guardar el tiempo de bloqueo
                                            Calendar cal = Calendar.getInstance();
                                            cal.setTime(new Date());
                                            cal.add(Calendar.MINUTE, blocMinutos);
                                            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                                            sesion.setAttribute("blocHasta", df.format(cal.getTime()));

                                            cookie.setMaxAge(blocMinutos * 60);
                                            response.addCookie(cookie);

                                        } else if (blocIntentos > 1) {
                                            blocIntentos--;
                                            sesion.setAttribute("blocIntentos", blocIntentos);
                                        }
                                    }
                                }
                            }
                            String msg = "Credenciales incorrectos.";
                            out.print("<body onload=\"alert('" + msg + "');\">");
                            response.setHeader("Refresh", "0;url=login.jsp");
                        }
                    }
                } else {
                    if (request.getHeader("Referer") != null) {
                        if (!request.getHeader("Referer").toString().contains("PrimeraVersion")) {
                            sesion.setAttribute("Referer", request.getHeader("Referer"));
                        }
                    }
                    boolean hayCookie = false;
                    Cookie[] allCookies = request.getCookies();
                    if (allCookies != null) {
                        for (Cookie cookie : allCookies) {
                            if (cookie.getName().equals("glrp")) {
                                hayCookie = true;
                                String usr = conBD.getUserByRememberPassCode(cookie.getValue());
                                //Buscar en bbdd si existe un codigo e iniciar sesion
                                if (usr != null) {
                                    sesion = request.getSession(true);
                                    sesion.setAttribute("User", usr);
                                    response.sendRedirect("ValidarLogin");
                                } else {
                                    cookie.setMaxAge(0);
                                    response.addCookie(cookie);
                                    response.sendRedirect("login.jsp");
                                }
                            }
                        }
                    }
                    if (!hayCookie) {
                        //Recogida de parametros de las aplicaciones
                        if (request.getParameter("register") != null) {
                            sesion.setAttribute("Register", request.getParameter("register"));
                        }
                        if (request.getParameter("blocIntentos") != null) {
                            sesion.setAttribute("blocIntentosOrig", request.getParameter("blocIntentos"));
                            sesion.setAttribute("blocIntentos", request.getParameter("blocIntentos"));
                        }
                        if (request.getParameter("blocMinutos") != null) {
                            sesion.setAttribute("blocMinutos", request.getParameter("blocMinutos"));
                        }
                        if (request.getParameter("blocIntentosUser") != null) {
                            sesion.setAttribute("blocIntentosUser", request.getParameter("blocIntentosUser"));
                        }

                        String msg = "No has iniciado sesión o tu sesión ha caducado...";
                        out.print("<body onload=\"alert('" + msg + "');\">");
                        response.setHeader("Refresh", "0;url=logued.jsp");
                    }
                }
            }
            out.println("\n");
            out.println("    </body>\n");
            out.println("</html>\n");
        }
    }

    //retorna true si hay un usuario a bloquear en la base de datos
    private boolean usuario2Bloquear(List<String> usuarios, int intentos, int blocMin, String app) {
        HashMap<String, Integer> hashmap = new HashMap();
        for (int i = 0; i < usuarios.size(); i++) {
            if (hashmap.get(usuarios.get(i)) != null) {
                hashmap.put(usuarios.get(i), hashmap.get(usuarios.get(i)) + 1);
            } else {
                hashmap.put(usuarios.get(i), 1);
            }
        }

        HashMap<String, Integer> sortedHashmap = (HashMap<String, Integer>) sortByValue(hashmap);

        if (sortedHashmap.containsValue(intentos)) {
            for (Map.Entry<String, Integer> entry : sortedHashmap.entrySet()) {
                System.out.println(entry.getKey());
                if (entry.getValue() == intentos) {
                    ConsultaBD conBD = new ConsultaBD();
                    String user = entry.getKey();
                    if (conBD.bloquearUsuarioAplicacion(user, app, blocMin)) {
                        //Envio de email informando del bloqueo
                        String receptor = ((User) conBD.getUserListByCriteria(user, "", "", "").get(0)).getEmail();
                        String asunto = "Bloqueo temporal - Global Login";
                        String body = getMessageBody(user, app);
                        EnviarMail gMail = new EnviarMail(receptor, asunto, body);
                        gMail.start();
                        return true;
                    }
                }

            }
        }

        System.out.println("map: " + hashmap);
        System.out.println("sorted map: " + sortedHashmap);
        return false;
    }

    static Map sortByValue(Map map) {
        List list = new LinkedList(map.entrySet());
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        Map result = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    private String getMessageBody(String usuario, String app) {
        String msg = "";

        msg += "Hola " + usuario + ",\n"
                + "\n";
        msg += "Su cuenta ha sido bloqueada de forma temporal, para la aplicación \"" + app + "\", a causa de un número excesivo de intentos de autentificación.\n";
        msg += "\n"
                + "Atentamente, \n"
                + "Equipo de Global Login.";

        return msg;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JOSEException ex) {
            Logger.getLogger(ValidarLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JOSEException ex) {
            Logger.getLogger(ValidarLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
