/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import BBDD.ConsultaBD;
import Utils.Util;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author WiSaM
 */
public class Edit extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Edit</title>");
            out.println("</head>");

            Util utilb = new Util();
            HttpSession sesion = request.getSession(false);
            String newUser = "";
            String newEmail = "";
            String newApp = "";
            String newPerfI = "";
            String newPerfE = "";
            String oldUser = "";
            String oldEmail = "";

            if (sesion.getAttribute("User") == null) {
                response.sendRedirect("login.jsp");
            } else if (!utilb.isAdmin((String) sesion.getAttribute("User"))) {
                response.sendRedirect("logued.jsp");
            } else {
                if (request.getParameter("submit") != null) {
                    oldUser = (request.getParameter("oldName") != null) ? request.getParameter("oldName") : "";
                    oldEmail = (request.getParameter("oldEmail") != null) ? request.getParameter("oldEmail") : "";
                    newUser = (request.getParameter("userName") != null) ? request.getParameter("userName") : "";
                    newEmail = (request.getParameter("email") != null) ? request.getParameter("email") : "";
                    newApp = (request.getParameter("app") != null && (!request.getParameter("app").equals("NS"))) ? request.getParameter("app") : "";
                    newPerfI = (request.getParameter("perfI") != null && (!request.getParameter("perfI").equals("0"))) ? request.getParameter("perfI") : "";
                    newPerfE = (request.getParameter("perfE") != null && (!request.getParameter("perfE").equals("0"))) ? request.getParameter("perfE") : "";

                    ConsultaBD conBD = new ConsultaBD();
                    if (1 == conBD.setUserByAdmin(oldUser, oldEmail, newUser, newEmail, newPerfI, newPerfE, newApp)) {
                        String msg = "Error! El nombre de usuario ya existe.. ";
                        out.print("<body onload=\"alert('" + msg + "');\">");
                        response.setHeader("Refresh", "0;url=cpanel.jsp");
                    } else {
                        String msg = "Datos modificados con éxito.";
                        out.print("<body onload=\"alert('" + msg + "');\">");
                        response.setHeader("Refresh", "0;url=cpanel.jsp");
                    }
                }else{
                    response.setHeader("Refresh", "0;url=logued.jsp");
                }
            }
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
