/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import BBDD.ConsultaBD;
import Model.Aplicacion;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author WiSaM
 */
public class Logout extends HttpServlet {

    private HttpSession session;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>\n");
            out.println("<html>\n");
            out.println("    <head>\n");
            out.println("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
            out.println("        <title>Logout Page</title>\n");
            out.println("    </head>\n");
            out.println("    <body>\n");

            session = request.getSession(false);

            Cookie[] allCookies = request.getCookies();
            if (allCookies != null) {
                for (Cookie cookie : allCookies) {
                    if (cookie.getName().equals("glrp")) {
                        cookie.setMaxAge(0);
                        response.addCookie(cookie);
                    }
                }
            }
            if (session != null) {
                if (session.getAttribute("User") != null) {
                    ConsultaBD conBD = new ConsultaBD();
                    conBD.putCodeRemember(session.getAttribute("User").toString(), "NO");
                }
                ConsultaBD conBD = new ConsultaBD();
                ArrayList<Aplicacion> apps = new ArrayList<Aplicacion>();
                apps = conBD.getAplicaciones();
                for (int i = 0; i < apps.size(); i++) {
                    out.println("<iframe src=\"" + apps.get(i).getUrl() + "logout.jsp\" hidden>");
                    out.println("</iframe>");
                }
                session.invalidate();
            }

            String msg = "Sesión cerrada correctamente..";
            out.print("<body onload=\"alert('" + msg + "');\">");
            response.setHeader("Refresh", "0;url=login.jsp");

            out.println("    </body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
