package Servlets;

import BBDD.ConsultaBD;
import Mail.EnviarMail;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import Utils.Util;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author WiSaM
 */
public class Forgot extends HttpServlet {
    
     static final int CONFIRM_CODE_LENGTH = 15;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ForgotA</title>");            
            out.println("</head>");
            
            String opcion = request.getParameter("opcion");
            if (opcion!=null){
            boolean pass = (opcion.equals("pass") || opcion.equals("all"));
            boolean user = (opcion.equals("username") || opcion.equals("all"));
            
            String receptor = request.getParameter("mail");
            ConsultaBD conBD = new ConsultaBD();
            if (opcion!=null){
                if (receptor!=null){
                    String username = conBD.getUserByEmail(receptor);
                    if (username != null) {
                        String asunto = "Recuperación de credenciales - Global Login";
                        String codePass = (new Util()).randomString(CONFIRM_CODE_LENGTH);
                        if (conBD.putCodePass(receptor, codePass)) {
                            String body = getMessageBody(user, pass, username, codePass);
                            EnviarMail gMail = new EnviarMail(receptor, asunto, body);
                            try{
                                gMail.start();
                                String msg = "Tus credenciales ya han sido enviados a tu correo.";
                                out.println("<body onload=\"alert('" + msg + "');\">");
                                //Redireccionamos a Login
                                response.setHeader("Refresh", "0;url=login.jsp");
                            }catch(Exception e){
                                out.println("<body>");
                                out.println("<p>Fallo al generar su email, puede ponerse en contacto con nosotros en "
                                        + "<a href=\"mailto:global.login.mail@gmail.com\">Global.Login.Mail@gmail.com</a></p>");
                            }
                        }
                    }else{
                        String msg = "El correo electronico introducido no corresponde a ningun usuario.";
                        out.println("<body onload=\"alert('" + msg + "');\">");
                        //Redireccionamos a Forgot
                        response.setHeader("Refresh", "0;url=forgot.jsp");
                    }
                }
            }
            }else{
                response.setHeader("Refresh", "0;url=login.jsp");
            }
            
            out.println("</body>");
            out.println("</html>");
        }
    }
    
     private String getMessageBody(boolean user, boolean pass, String usuario, String codigoPass) {
        String msg = "";

        msg += "Hola,\n"
                + "\n";

        if (user) {
            msg += "tus credenciales son:\n";
            msg += "\tUser: " + usuario + "\n";
        }
        if (pass) {
            msg += "haz click en el siguiente enlace para generar una nueva contraseña:\n";
            msg += "\t" + "http://localhost:8080/PrimeraVersion/changePass.jsp?codePass=" + codigoPass + "\n";
        }
        msg
                += "\n"
                + "Atentamente, \n"
                + "Equipo de Global Login.";

        return msg;
    }

 


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
