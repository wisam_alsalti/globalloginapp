/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import BBDD.ConsultaBD;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author WiSaM
 */
@WebServlet(name = "ChangePass", urlPatterns = {"/ChangePass"})
public class ChangePass extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>\n");
            out.println("<html>\n");
            out.println("    <head>\n");
            out.println("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
            out.println("        <title>Cambiar Password</title>\n");
            out.println("    </head>\n");
            out.println("        ");

            ConsultaBD conBD = new ConsultaBD();
            String confirmCode = request.getParameter("cambioPassCode");
            String pass = request.getParameter("pass");
            String pass1 = request.getParameter("pass1");
            String passAct = request.getParameter("passAct");
            boolean passActual = false;
            HttpSession sesion = request.getSession(false);
            if (passAct != null) {
                if (sesion.getAttribute("User") != null) {
                    passActual = (1 == conBD.comprovarLogin(sesion.getAttribute("User").toString(), DigestUtils.md5Hex(passAct.trim())));
                    if (!passActual) {
                        String msg = "La contraseña actual no es correcta.";
                        out.print("<body onload=\"alert('" + msg + "');\">");
                    }
                }
            }
            if (pass != null && pass.equals(pass1)) {
                if (confirmCode != null) {
                    if (!confirmCode.equals("OK")) {
                        if (conBD.cambiarPass(confirmCode, null, DigestUtils.md5Hex(pass.trim()))) {
                            String msg = "Tu contraseña se ha cambiado correctamente.";
                            out.print("<body onload=\"alert('" + msg + "');\">");
                            //Redireccionamos a Forgot
                            response.setHeader("Refresh", "0;url=login.jsp");
                        } else {
                            out.println("<p>Fallo con tu código de cambio de password, solicita un nuevo código o pongase en contacto con nosotros en el siguiente email: "
                                    + "<a href=\"mailto:global.login.mail@gmail.com\">Global.Login.Mail@gmail.com</a></p>");
                        }
                    }
                } else if (sesion != null && passActual) {
                    String username = (String) sesion.getAttribute("User");
                    if (username != null) {
                        if (conBD.cambiarPass(null, username, DigestUtils.md5Hex(pass.trim()))) {
                            String msg = "Tu contraseña se ha cambiado correctamente.";
                            out.print("<body onload=\"alert('" + msg + "');\">");
                            sesion.invalidate();
                            Cookie[] allCookies = request.getCookies();
                            if (allCookies != null) {
                                for (Cookie cookie : allCookies) {
                                    if (cookie.getName().equals("glrp")) {
                                        cookie.setMaxAge(0);
                                        response.addCookie(cookie);
                                    }
                                }
                            }
                            response.setHeader("Refresh", "0;url=logued.jsp");
                        } else {
                            out.println("<p>Fallo al cambiar su contraseña, puede ponerse en contacto con nosotros en "
                                    + "<a href=\"mailto:global.login.mail@gmail.com\">Global.Login.Mail@gmail.com</a></p>");
                        }
                    }
                } else {
                    response.setHeader("Refresh", "0;url=changePass.jsp");
                }
            } else if (pass == null || pass1 == null){
                response.setHeader("Refresh", "0;url=login.jsp");
            } else{
                    String msg = "Las contraseñas no coinciden.";
                    out.print("<body onload=\"alert('" + msg + "');\">");
                if (confirmCode != null) {
                    response.setHeader("Refresh", "0;url=changePass.jsp?codePass=" + confirmCode);
                }else{
                    response.setHeader("Refresh", "0;url=opciones.jsp?act=cambiaPass");
                }
            }

            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
