/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import BBDD.ConsultaBD;
import Mail.EnviarMail;
import Utils.Util;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author WiSaM
 */
public class Register extends HttpServlet {

    static final int CONFIRM_CODE_LENGTH = 15;
    static final String CONFIRM_URL = "http://localhost:8080/PrimeraVersion/Confirm?confirmCode=";
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String username = request.getParameter("userName");
            String pass = request.getParameter("pass");
            String pass1 = request.getParameter("pass1");
            String email = request.getParameter("email");
            String hint = request.getParameter("hint");
            String app = request.getParameter("app");

            if (username==null || pass==null || pass1==null || email==null) {
                response.sendRedirect("register.jsp");
            } else {
                //TODO: elses...
                if (pass.equals(pass1)) {
                    ConsultaBD conBD = new ConsultaBD();
                    int ret = conBD.comprovarUserRegister(username, email);
                    if (ret == 2) {
                        String msg = "El email introducido ya está en uso.";
                                out.println("<body onload=\"alert('" + msg + "');\">");
                                response.setHeader("Refresh", "0;url=register.jsp");
                    } else if (ret == 1) {
                        String msg = "El nombre de usuario introducido ya está en uso.";
                                out.println("<body onload=\"alert('" + msg + "');\">");
                                response.setHeader("Refresh", "0;url=register.jsp");
                    } else {
                        //Enviamos email de confirmacion
                        String receptor = email;
                        String asunto = "Confirmación registro Global Login";
                        Util util = new Util();
                        String confirmCode = util.randomString(CONFIRM_CODE_LENGTH);
                        String body = getRegisterMessageBody(username, pass, confirmCode);
                        EnviarMail gMail = new EnviarMail(receptor, asunto, body);
                        gMail.start();
                        if (conBD.nuevoRegistro(username, DigestUtils.md5Hex(pass.trim()), email, confirmCode, hint)){
                            //suscribir usuario a aplicacion
                            conBD.suscribirUsuarioAplicacion(username, app);
                            String msg = "Registrado correctamente, consulte su email para confirmar su registro.";
                                out.println("<body onload=\"alert('" + msg + "');\">");
                                response.setHeader("Refresh", "0;url=login.jsp");
                        }
                    }
                }else{
                    String msg = "Las contraseñas no coinciden.";
                                out.println("<body onload=\"alert('" + msg + "');\">");
                                response.setHeader("Refresh", "0;url=register.jsp");
                
                }
            }
        }
    }
    
    private String getRegisterMessageBody(String user, String pass, String code){
        String msg = "";
        
        msg += "Hola,\n"
                + "\n"
                + "tus credenciales son:\n"
                + "\tUser: " + user + "\n"
                + "\tPassword: " + pass + "\n"
                + "\n"
                + "Link de confirmación: " + CONFIRM_URL + code + " \n"
                + "\n"
                + "Atentamente, \n"
                + "Equipo de Global Login.";
        
        return msg;
    }
    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
