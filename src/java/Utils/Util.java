/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import BBDD.ConsultaBD;
import java.util.Random;

/**
 *
 * @author WiSaM
 */
public class Util {

    public Util() {
    }

    public String getUserByEmail(String email) {
        ConsultaBD conBD = new ConsultaBD();
        String username = conBD.getUserByEmail(email);
        return username;
    }

    public String getHintByUser(String username) {
        ConsultaBD conBD = new ConsultaBD();
        String hint = conBD.getHintByUser(username);
        return hint;
    }

    public String randomString(int num) {
        String[] all = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"};
        //26+26+10=62
        String msg = "";
        //seed --> (timeInMillis mod 1000)
        long timeInMillis = (new java.util.GregorianCalendar().getTimeInMillis()) % 1000;
        Random random = new Random(timeInMillis);
        for (int i = 0; i < num; i++) {
            int rand = (random.nextInt(1000)) % 62;
            msg += all[rand];
        }
        return msg;
    }

    public String getInternalPerfil(String user) {
        String perf;
        ConsultaBD conBD = new ConsultaBD();
        int num = conBD.getInternalPerfilByUser(user);
        
        switch (num) {
            case 1:
                perf = "FREE";
                break;
            case 2:
                perf = "PREM";
                break;
            case 4:
                perf = "APP";
                break;
            case 15:
                perf = "ADMIN";
                break;
            default:
                perf = "DESC";
                break;
        }
        return perf;
    }

    public String getExternalPerfil(String user) {
        ConsultaBD conBD = new ConsultaBD();
        return Integer.toString(conBD.getExternalPerfilByUser(user));
    }
    
    public boolean isAdmin(String user) {
        return getInternalPerfil(user).equals("ADMIN");
    }
    
    public boolean isApp(String user) {
        return getInternalPerfil(user).equals("APP");
    }

}
